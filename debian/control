Source: python-corner
Maintainer: Debian PaN Maintainers <debian-pan-maintainers@alioth-lists.debian.net>
Uploaders:
 Debian Python Team <team+python@tracker.debian.org>,
 Roland Mas <lolando@debian.org>
Section: python
Priority: optional
Build-Depends: dh-python, python3-setuptools, python3-all,
 debhelper (>= 12),
 python3-matplotlib,
 pybuild-plugin-pyproject,
 python3-setuptools-scm,
 python3-numpy,
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/python-team/packages/python-corner.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-corner
Homepage: https://github.com/dfm/corner.py

Package: python3-corner
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: corner.py - make beautiful corner plots
 This Python module uses matplotlib to visualize multidimensional
 samples using a scatterplot matrix. In these visualizations, each
 one- and two-dimensional projection of the sample is plotted to
 reveal covariances. corner was originally conceived to display the
 results of Markov Chain Monte Carlo simulations and the defaults are
 chosen with this application in mind but it can be used for
 displaying many qualitatively different samples.
 .
 Development of corner happens on GitHub so you can raise any issues
 you have there. corner has been used extensively in the astronomical
 literature and it has occasionally been cited as corner.py or using
 its previous name triangle.py.
